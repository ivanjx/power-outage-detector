#include <Arduino.h>

const int SERIAL_BAUD_RATE = 9600;
const int INPUT_PIN = 10;
const int COMMAND_BUFF_LEN = 1024;

char _commandBuff[COMMAND_BUFF_LEN];

void setup() {
  // Setting up serial comm.
  Serial.begin(SERIAL_BAUD_RATE);
  Serial.println("Initialized serial comm.");

  // We dont actually have to do this:
  // Setting pin mode to INPUT.
  pinMode(INPUT_PIN, INPUT);
  Serial.println("Initialized input pin.");
}

void clearBuffer(char buff[], int buffLen) {
  for(int i = 0; i < buffLen; ++i) {
    buff[i] = 0;
  }
}

int readSerialLine(char buff[], int buffLen) {
  int totalRead = 0;

  while(true) {
    if(!Serial.available()) {
      // Read when ready.
      continue;
    }

    // Reading a char.
    char read = Serial.read();
    
    // Checking.
    if(read == 0) {
      // Ignore NULL.
      continue;
    }

    if(read == '\n') {
      // Done.
      break;
    }

    buff[totalRead] = read;
    ++totalRead;

    if(buffLen == totalRead) {
      // Enough is enough.
      break;
    }
  }

  return totalRead;
}

void handleUnknown(char cmd[]) {
  // Sending response.
  Serial.print("Unknown command: ");
  Serial.println(cmd);
}

void handleHELO() {
  // Sending response.
  Serial.println("HELO");
}

void handleREAD() {
  // Reading input pin.
  if(digitalRead(INPUT_PIN) == HIGH) {
    // Power is connected.
    Serial.println("HIGH");
  } else {
    // Power is disconnected.
    Serial.println("LOW");
  }
}

void loop() {
  // Clearing buffer.
  clearBuffer(_commandBuff, COMMAND_BUFF_LEN);

  // Reading command.
  int read = readSerialLine(_commandBuff, COMMAND_BUFF_LEN);

  if(read == 0) {
    // Just ignore.
    return;
  }

  // Parsing command.
  if(strcmp(_commandBuff, "HELO") == 0) {
    handleHELO();
  } else if(strcmp(_commandBuff, "READ") == 0) {
    handleREAD();
  } else {
    handleUnknown(_commandBuff);
  }
}